Usual with sloppy terms:

```
{!boost b=boost_document} ((+(tm_X3b_en_rendered_item:"Cambridge Public Health"^2 tm_X3b_und_rendered_item:"Cambridge Public Health"^2) +(tm_X3b_en_rendered_item:"Cambridge Public"^2 tm_X3b_und_rendered_item:"Cambridge Public"^2) +(tm_X3b_en_rendered_item:"Public Health"^2 tm_X3b_und_rendered_item:"Public Health"^2) +(tm_X3b_en_rendered_item:"Cambridge"^2 tm_X3b_und_rendered_item:"Cambridge"^2) +(tm_X3b_en_rendered_item:"Public"^2 tm_X3b_und_rendered_item:"Public"^2) +(tm_X3b_en_rendered_item:"Health"^2 tm_X3b_und_rendered_item:"Health"^2)) tm_X3b_en_rendered_item:(+"Cambridge Public Health"~10000000 +"Cambridge Public"~10000000 +"Public Health"~10000000 +"Cambridge" +"Public" +"Health")^2 tm_X3b_und_rendered_item:(+"Cambridge Public Health"~10000000 +"Cambridge Public"~10000000 +"Public Health"~10000000 +"Cambridge" +"Public" +"Health")^2)
```

Getting rid of sloppy terms still left the plus signs which, in the duplicate second query(?!?!?)'s configuration (the ones that put parenthesis around multiple terms) causes it to filter out anything that does not have *all* terms.  Passing in the #conjunction 'OR' fixes the problem, though not the weird duplication:

```
{!boost b=boost_document} (((tm_X3b_en_rendered_item:"\"Cambridge Public Health\""^2 tm_X3b_und_rendered_item:"\"Cambridge Public Health\""^2) (tm_X3b_en_rendered_item:"\"Cambridge Public\""^2 tm_X3b_und_rendered_item:"\"Cambridge Public\""^2) (tm_X3b_en_rendered_item:"\"Public Health\""^2 tm_X3b_und_rendered_item:"\"Public Health\""^2) (tm_X3b_en_rendered_item:"Cambridge"^2 tm_X3b_und_rendered_item:"Cambridge"^2) (tm_X3b_en_rendered_item:"Public"^2 tm_X3b_und_rendered_item:"Public"^2) (tm_X3b_en_rendered_item:"Health"^2 tm_X3b_und_rendered_item:"Health"^2)) tm_X3b_en_rendered_item:("\"Cambridge Public Health\"" "\"Cambridge Public\"" "\"Public Health\"" "Cambridge" "Public" "Health")^2 tm_X3b_und_rendered_item:("\"Cambridge Public Health\"" "\"Cambridge Public\"" "\"Public Health\"" "Cambridge" "Public" "Health")^2)
```


This is with no parse mode set, which falls back to 'terms' (Multiple terms).


All the action is really in:

web/modules/contrib/search_api_solr/src/Utility/Utility.php


Anyway here is what we presently live with, should i be worried about the escaped quotation marks that i've somehow introduced:

```
{!boost b=boost_document} (((tm_X3b_en_rendered_item:"\"Cambridge Public Health\""^2 tm_X3b_und_rendered_item:"\"Cambridge Public Health\""^2) (tm_X3b_en_rendered_item:"\"Cambridge Public\""^2 tm_X3b_und_rendered_item:"\"Cambridge Public\""^2) (tm_X3b_en_rendered_item:"\"Public Health\""^2 tm_X3b_und_rendered_item:"\"Public Health\""^2) (tm_X3b_en_rendered_item:"Cambridge"^2 tm_X3b_und_rendered_item:"Cambridge"^2) (tm_X3b_en_rendered_item:"Public"^2 tm_X3b_und_rendered_item:"Public"^2) (tm_X3b_en_rendered_item:"Health"^2 tm_X3b_und_rendered_item:"Health"^2)) tm_X3b_en_rendered_item:("\"Cambridge Public Health\""~10000000 "\"Cambridge Public\""~10000000 "\"Public Health\""~10000000 "Cambridge" "Public" "Health")^2 tm_X3b_und_rendered_item:("\"Cambridge Public Health\""~10000000 "\"Cambridge Public\""~10000000 "\"Public Health\""~10000000 "Cambridge" "Public" "Health")^2)
```

Answer, yes i should it was breaking things.  A clean output by not adding them:

```
{!boost b=boost_document} (((tm_X3b_en_rendered_item:"Cambridge Public Health"^2 tm_X3b_und_rendered_item:"Cambridge Public Health"^2) (tm_X3b_en_rendered_item:"Cambridge Public"^2 tm_X3b_und_rendered_item:"Cambridge Public"^2) (tm_X3b_en_rendered_item:"Public Health"^2 tm_X3b_und_rendered_item:"Public Health"^2) (tm_X3b_en_rendered_item:"Cambridge"^2 tm_X3b_und_rendered_item:"Cambridge"^2) (tm_X3b_en_rendered_item:"Public"^2 tm_X3b_und_rendered_item:"Public"^2) (tm_X3b_en_rendered_item:"Health"^2 tm_X3b_und_rendered_item:"Health"^2)) tm_X3b_en_rendered_item:("Cambridge Public Health"~10000000 "Cambridge Public"~10000000 "Public Health"~10000000 "Cambridge" "Public" "Health")^2 tm_X3b_und_rendered_item:("Cambridge Public Health"~10000000 "Cambridge Public"~10000000 "Public Health"~10000000 "Cambridge" "Public" "Health")^2)
```


If someone uses quotation marks in their search, it doesn't immediately look bad in results but there are far fewer but *not* only ones that have the phrase, so… it is bad, power user phrase search is not working.

```
{!boost b=boost_document} (((tm_X3b_en_rendered_item:"\"Cambridge Public Health\""^2 tm_X3b_und_rendered_item:"\"Cambridge Public Health\""^2) (tm_X3b_en_rendered_item:"\"Cambridge Public"^2 tm_X3b_und_rendered_item:"\"Cambridge Public"^2) (tm_X3b_en_rendered_item:"Public Health\""^2 tm_X3b_und_rendered_item:"Public Health\""^2) (tm_X3b_en_rendered_item:"\"Cambridge"^2 tm_X3b_und_rendered_item:"\"Cambridge"^2) (tm_X3b_en_rendered_item:"Public"^2 tm_X3b_und_rendered_item:"Public"^2) (tm_X3b_en_rendered_item:"Health\""^2 tm_X3b_und_rendered_item:"Health\""^2)) tm_X3b_en_rendered_item:("\"Cambridge Public Health\""~10000000 "\"Cambridge Public"~10000000 "Public Health\""~10000000 "\"Cambridge" "Public" "Health\"")^2 tm_X3b_und_rendered_item:("\"Cambridge Public Health\""~10000000 "\"Cambridge Public"~10000000 "Public Health\""~10000000 "\"Cambridge" "Public" "Health\"")^2)
```
