<?php

namespace Drupal\drutopia_findit_search;

/**
 * Shared common traits for hacky markup of occurrences (each with one time).
 *
 * @internal
 */
trait MicroOccurrenceMarkupTrait {

  /**
   * Output our HTML in ugliest way possible just about.
   *
   * Mimics what we got out of micro template.
   *
   * Cannot use micro node format because we need to use the correct time
   * of a given occurrence (each node has multiple occurrences).
   *
   * @TODO figure out how to do this in a node template, or create our own template.
   */
  public static function renderDay($occurences) {
    $rd = '';
    foreach ($occurences as $occurence) {
      $rd .= '<a href="';
      $rd .= $occurence['url'];
      $rd .= '" class="findit-event micro columns">';
      $rd .= '<h2 class="column is-three-quarters is-size-3">';
      $rd .= $occurence['title'];
      $rd .= '</h2>';
      $rd .= '<h5 class="column is-one-quarter is-size-3">';
      $rd .= $occurence['time'];
      $rd .= '</h5>';
      $rd .= '</a>';
    }
    return $rd;
  }

  /**
   * @TODO: The result never changes now so we may want to rethink this.
   */
  public static function getDaysOccurrences($result, $date_start, $date_end) {
    $occurences = [];
    /* Drupal\search_api\Item\Item; */
    foreach ($result->getResultItems() as $item) {
      $node = $item->getOriginalObject()->getValue();
      $ranges = $node->get("field_findit_opportunity_dates")->getValue();
      if ($time = self::timeIfHappensOnDay($ranges, $date_start, $date_end)) {
        $occurences[] = [
          'title' => $node->label(),
          'url' => $node->toUrl()->toString(),
          'time' => $time,
        ];
      }
    }
    return $occurences;
  }

  public static function timeIfHappensOnDay($ranges, $date_start, $date_end) {
    foreach($ranges as $range) {
      if ($date_end >= $range['value'] && $range['end_value'] >= $date_start) {
        $time = strftime("%l:%M%P", $range['value']);
        $time .= ' – ';
        $time .= strftime("%l:%M%P", $range['end_value']);
        return $time;
      }
    }
    return FALSE;
  }

}
