<?php


namespace Drupal\drutopia_findit_search\Controller;

use DateTime;
use DateTimeZone;
use Drupal\node\NodeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\drutopia_findit_search\MicroOccurrenceMarkupTrait;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Query\Query;


class UpcomingEventsController extends ControllerBase {

  use MicroOccurrenceMarkupTrait;

  const DRUPAL_DATE_FORMAT = 'Y-m-d\Th:i:s';

  /**
   * @var \Drupal\search_api\Query
   */
  protected $query;

  /**
   * Returns search results.
   *
   * Executes solr query using
   *
   * @return array
   * @throws \Drupal\search_api\SearchApiException
   */
  public function list() {

    // How long a period to show events for daily.
    $days = 90;

    // We only have this because the hidden default query range (if no range is
    // set) is 10, so we think we may need to work around a lower-than-desired
    // limit.
    $max_events = 500;

    $build = [];

    $timezone = \Drupal::config('system.date')->get('timezone.default');

    $day = \Drupal::request()->query->get('day');
    if (!$day) {
      $date_start = new DateTime('today', new DateTimeZone($timezone));
    }
    else {
      $date_start = new DateTime($day, new DateTimeZone($timezone));
    }
    // Confirmed this works the same as making a new datetime object like this:
    // $utc_date = new DateTime($date_start->format('@U'), new DateTimeZone('UTC'));
    $date_start->setTime(0, 0, 0);
    $date_start->setTimezone(new DateTimeZone('UTC'));

    $date_end = clone($date_start);
    $date_end->modify("+$days days");

    // couldn't get this to work
    // $result = \Drupal::service('drutopia_findit_search.upcoming_events')->getResults($date);
    // So we're doing all the crazy stuff.
    // @TODO that actually worked fine so move the query stuff back there, and
    // leave the crazy foreaching here (or in a helper function/method here.

    $this->query = new Query(Index::load('main'));
    $this->query->range(0, $max_events);
    $this->query->sort('findit_next_date', 'ASC');
    $this->query->sort('title', 'ASC');
    $this->query->addCondition('status', NodeInterface::PUBLISHED);
    $this->query->addCondition('types', 'findit_event', 'IN');

    // Logically, we want an event that occurs on our given day, but we cannot
    // get multivalue date range queries to work that way (to keep trying, see
    // https://agaric.gitlab.io/raw-notes/notes/2020-12-04-filtering-by-multivalue-date-range-fields-in-solr/
    // )
    // Because recurring events don't work with this (any?) Solr query.
    // We need to be super-inclusive, and then filter out with PHP :lolsob:
    // Therefore, we're doing ridiculous filtering in ->itemHappensOnDay().

    // Even worse, for unknown reasons (not actually stored as UTC?) this
    // isn't giving the right results, even though it totally should.
    // $this->query->addCondition('dates', $date_start->format(DATE_ISO8601), '<=');
    // $this->query->addCondition('dates_end', $date_end->format(DATE_ISO8601), '>=');

    $this->query->addCondition('findit_next_date', $date_start->format(DATE_ISO8601), '>=');
    // $this->query->addCondition('findit_last_date', $date_end->format(DATE_ISO8601), '<=');

    // @TODO see if not executing query every time can possibly work for this.
    // We're executing this multiple times on a given page.
    $result = $this->query->execute();

    if ($result->getResultCount() > $max_events) {
      $build['limit_exceeded_warning'] = ['#markup' => '<p>' . t("There are more than :max events in the period from :start through :end, and not all can be displayed.  Please <a href='mailto:ask@agaric.coop'>contact the web developer</a> to rearchitect this page.", [':max' => $max_events, ':start' => $date_start->format('l, F jS'), ':end' => $date_end->format('l, F jS')]) . '</p>'];
    }

    for ($i = 0; $i < 90; $i++) {

      $day_start = clone($date_start);
      $day_start->modify("+$i days");

      $day_end = clone($day_start);
      $day_end->modify('+1 day');


      $build['title' . $i] = [
        '#markup' => '<h3 class="is-size-3"><em>' . $day_start->format('l, F jS') . '</em></h3>',
      ];
      $occurences = MicroOccurrenceMarkupTrait::getDaysOccurrences($result, $day_start->getTimestamp(), $day_end->getTimestamp());
      if ($occurences) {
        $build['body' . $i] = [
          '#markup' => MicroOccurrenceMarkupTrait::renderDay($occurences),
        ];
      }
      else {
        $build['none_today' . $i] = [
          '#markup' => '<div class="findit-event micro columns"><p class="column">' . t('No events.') . '</p></div>',
        ];
      }

    }

    $build['filter_events_link'] = [
      '#markup' => '<h3 class="title">' . '<a href="/events">' . t('Search and filter events.') . '</a>' . '</h3>',
    ];
    return $build;

  }

  public function title() {
    $month = \Drupal::request()->query->get('month');

    if (!$month) {
      return t('Upcoming events');
    }

  }

}
