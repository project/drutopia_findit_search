<?php


namespace Drupal\drutopia_findit_search\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\search_api\Item\Item;


class SearchController extends ControllerBase {

  /**
   * Returns search results.
   *
   * Executes solr query using
   *
   * @return array
   * @throws \Drupal\search_api\SearchApiException
   */
  public function search() {
    $result = \Drupal::service('drutopia_findit_search.prepared_query')->getResults();
    if ($result->getResultCount() == 0) {
      return [
        '#markup' => t('No search results found. Please try again with different keywords or filter settings.'),
      ];
    }
    else {
      $build = [];
      if ($description = $this->description()) {
        $build['description'] = ['#markup' => $description];
      }
      $map = function (Item $item) {
        $opportunity = $item->getOriginalObject()->getValue();
        // Add the score and boost so admins and developers can gain greater insight into how it's working.
        $opportunity->solr_score = $item->getScore();
        $opportunity->solr_boost = $item->getBoost();
        return $opportunity;
      };
      $build['results'] = \Drupal::entityTypeManager()->getViewBuilder('node')->viewMultiple(array_map($map, $result->getResultItems()), 'teaser');
      $build['pager'] = [
        '#type' => 'pager',
        '#element' => 1,
        '#weight' => 5,
      ];
      return $build;
    }
  }

  /**
   * Return the activity or service if there's only one of them.
   */
  public function oneActivityOrService() {
    $types = \Drupal::request()->query->get('types');

    if (!$types || count($types) === 4) {
      $services = \Drupal::request()->query->get('services');
      if ($services && count($services) === 1) {
        return $services[0];
      }
      $activities = \Drupal::request()->query->get('activities');
      if ($activities && count($activities) === 1) {
        return $activities[0];
      }
    }
    return NULL;
  }

  /**
   * TODO: We could check that the term we're returning is in the vocabulary
   * we're checking (handle edge case of activity with same name as service).
   */
  public function description() {
    $term_name = $this->oneActivityOrService();
    if (!$term_name) {
      return '';
    }
    $terms = taxonomy_term_load_multiple_by_name($term_name);
    if (!$terms) {
      return '';
    }
    $term = array_shift($terms);
    return $term->description->value;
  }

  /**
   * Method to return a title used in the service _title_callback().
   */
  public function title() {
    $types = \Drupal::request()->query->get('types');

    if (!$types || count($types) === 4) {
      $term_name = $this->oneActivityOrService();
      if ($term_name) {
        return $term_name;
      }
      else {
        return t('Search');
      }
    }

    if (count($types) === 2) {
      if (in_array('findit_event', $types) && in_array('findit_program', $types)) {
        return t('Programs and Events');
      }
      if (in_array('findit_place', $types) && in_array('findit_program', $types)) {
        return t('Programs and Places');
      }
      if (in_array('findit_event', $types) && in_array('findit_place', $types)) {
        return t('Events and Places');
      }
      if (in_array('findit_organization', $types) && in_array('findit_program', $types)) {
        return t('Programs and Organizations');
      }
      if (in_array('findit_organization', $types) && in_array('findit_event', $types)) {
        return t('Events and Organizations');
      }
      if (in_array('findit_organization', $types) && in_array('findit_place', $types)) {
        return t('Places and Organizations');
      }
    }

    if (count($types) === 3) {
      if (in_array('findit_event', $types) && in_array('findit_program', $types) && in_array('findit_place', $types)) {
        return t('Places, Programs, and Events');
      }
      if (in_array('findit_place', $types) && in_array('findit_program', $types) && in_array('findit_organization', $types)) {
        return t('Organizations, Programs, and Places');
      }
      if (in_array('findit_event', $types) && in_array('findit_program', $types) && in_array('findit_organization', $types)) {
        return t('Organizations, Programs, and Events');
      }
      if (in_array('findit_event', $types) && in_array('findit_place', $types) && in_array('findit_organization', $types)) {
        return t('Organizations, Events, and Places');
      }
    }

    $type = current($types);
    switch ($type) {
      case 'findit_event':
        return t('Events');
      case 'findit_program':
        return t('Programs');
      case 'findit_organization':
        return t('Organizations');
      case 'findit_place':
        return t('Places');
      default:
        return t('Search');
    }

  }

}
