<?php


namespace Drupal\drutopia_findit_search;


use DateTime;
use DateTimeZone;
use Drupal\node\NodeInterface;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Query\Query;


class TodaysEvents {

  const LIMIT = 50;

  /**
   * @var \Drupal\search_api\Query
   */
  protected $query;

  public $today_start;
  public $today_end;

  public function __construct($index_id) {
    /* @var $pager_parameters \Drupal\Core\Pager\PagerParametersInterface */
    $pager_parameters = \Drupal::service('pager.parameters');
    $page = $pager_parameters->findPage(0);

    // We're supposed to show the events happening *today* even if they are
    // past, so this can't be relative to the current time but the actual day.
    $timezone = \Drupal::config('system.date')->get('timezone.default');
    $this->today_start = new DateTime('today', new DateTimeZone($timezone));
    $this->today_end =  new DateTime('tomorrow', new DateTimeZone($timezone));

    $this->query = new Query(Index::load($index_id));
    // $this->query->addCondition('dates_end', date(DATE_ISO8601), '>');
    $this->query->addCondition('findit_next_date', $this->today_start->format(DATE_ISO8601), '>=');
    $this->query->addCondition('findit_next_date', $this->today_end->format(DATE_ISO8601), '<');
    $this->query->sort('findit_next_date', 'ASC');
    $this->query->sort('title', 'ASC');
    $this->query->range($page * self::LIMIT, self::LIMIT);
    $this->query->addCondition('status', NodeInterface::PUBLISHED);
    $this->query->addCondition('types', 'findit_event', 'IN');

  }

  /**
   * Returns search results of this query.
   *
   * The entities found by this query are bulk loaded.
   *
   * @return \Drupal\search_api\Query\ResultSetInterface
   * @throws \Drupal\search_api\SearchApiException
   */
  public function getResults() {
    if (!$this->query->hasExecuted()) {
      $results = $this->query->execute();
      $results->preLoadResultItems();
    }
    else {
      $results = $this->query->getResults();
    }
    return $results;
  }

}
