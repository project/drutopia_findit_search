<?php


namespace Drupal\drutopia_findit_search\Form;


use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\drutopia_findit_search\PreparedQuery;
use Drupal\selectize\Element\Selectize;
use Symfony\Component\DependencyInjection\ContainerInterface;


class FiltersForm extends FormBase {

  /**
   * @var PreparedQuery
   */
  protected $query;

  /**
   * @var EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * FiltersForm constructor.
   *
   * @param PreparedQuery $query
   *   The query for the current request.
   * @param EntityFieldManagerInterface $entityFieldManager
   *   The EntityFieldManager.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The EntityTypeManager.
   */
  public function __construct(PreparedQuery $query, EntityFieldManagerInterface $entityFieldManager, EntityTypeManagerInterface $entityTypeManager) {
    $this->query = $query;
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('drutopia_findit_search.prepared_query'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'findit_search_filters_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $definitions = $this->entityFieldManager->getFieldStorageDefinitions('node');

    $inline_submit = $this->inlineSubmit();
    $standard_form_element = $this->standardElementBase();
    $selectize_standard_settings = $this->standardSelectizeSettings();

    $count = $this->query->getResults()->getResultCount();
    if ($count) {
      $form['result_count'] = [
        '#type' => 'markup',
        '#markup' => '<p class="drum">' . $this->formatPlural($count, "One opportunity.", "@count opportunities.") . '</p>',
      ];
    }
    else {
      $form['no_results'] = [
        '#type' => 'markup',
        '#markup' => '<p class="drum">' . $this->t( 'No opportunities found; <a href="/search">reset results</a>.') . '</p>',
      ];
    }

    $form['#method'] = 'get';
    $form['keywords'] = [
      '#type' => 'search',
      '#title' => t('Search'),
      '#attributes' => [
        'placeholder' => t('Search for events, programs, and more'),
      ],
      '#default_value' => $this->getRequest()->query->get('keywords'),
      '#field_suffix' => $inline_submit,
    ];


    if ($this->isDateContaining()) {
      // Daterange filtering.
      $form['start'] = [
        '#type' => 'date',
        '#title' => t('Occurs on or after'),
        '#default_value' => $this->getRequest()->query->get('start'),
        '#field_suffix' => $inline_submit,
      ];
      $form['end'] = [
        '#type' => 'date',
        '#title' => t('Occurs on or before'),
        '#default_value' => $this->getRequest()->query->get('end'),
        '#field_suffix' => $inline_submit,
      ];
    }

    $activities_options = $this->buildOptions('activities');
    if (!empty($activities_options)) {
      $form['activities'] = [
          '#settings' => Selectize::settings([
              'placeholder' => t('Select one or more activities'),
              'options' => $activities_options,
            ] + $selectize_standard_settings),
          '#title' => t('Activities'),
          '#options' => $this->simpleArrayFromValues($activities_options),
          '#default_value' => $this->getRequest()->query->get('activities'),
        ] + $standard_form_element;
    }

    $services_options = $this->buildOptions('services');
    if (!empty($services_options)) {
      $form['services'] = [
          '#settings' => Selectize::settings([
              'plugins' => ['remove_button'],
              'placeholder' => t('Select one or more services'),
            ] + $selectize_standard_settings),
          '#title' => t('Services'),
          '#options' => $this->simpleArrayFromValues($services_options),
          '#default_value' => $this->getRequest()->query->get('services'),
        ] + $standard_form_element;
    }

    $ages_options = $this->buildOptions('ages');
    if (!empty($ages_options)) {
      $form['ages'] = [
        '#settings' => Selectize::settings([
          'placeholder' => t('Select one or more ages'),
          'options' => $ages_options,
        ] + $selectize_standard_settings),
        '#title' => $this->t('Ages'),
        '#options' => $this->simpleArrayFromValues($ages_options),
        '#default_value' => $this->getRequest()->query->get('ages'),
      ] + $standard_form_element;
    }

    $cost_subsidies_values = array_unique(array_merge(array_column($this->query->getFacetMatchesByField('cost_subsidies'), 0), $this->getRequest()->query->get('cost_subsidies', [])));
    $cost_subsidies_options = array_intersect_key($definitions['field_findit_cost_subsidies']->getSetting('allowed_values'), array_flip($cost_subsidies_values));
    if (!empty($cost_subsidies_options)) {
      $form['cost_subsidies'] = [
        '#settings' => Selectize::settings([
          'placeholder' => t('Filter for free or reduced price'),
        ] + $selectize_standard_settings),
        '#title' => t('Cost'),
        '#options' => $cost_subsidies_options,
        '#default_value' => $this->getRequest()->query->get('cost_subsidies'),
      ] + $standard_form_element;
    }

    $this->addVirtual($form);

    $neighborhood_values = array_unique(array_merge(array_column($this->query->getFacetMatchesByField('neighborhoods'), 0), $this->getRequest()->query->get('neighborhoods', [])));
    $neighborhood_options = array_combine($neighborhood_values, $neighborhood_values);
    $form['neighborhoods'] = [
      '#type' => 'svg_select',
      '#title' => t('Neighborhoods'),
      '#title_display' => 'invisible',
      '#options' => $neighborhood_options,
      '#multiple' => TRUE,
      '#svg' => drupal_get_path('module', 'drutopia_findit_search') . '/images/cambridge-simplified-map.svg',
      '#default_value' => $this->getRequest()->query->get('neighborhoods'),
      '#field_suffix' => $inline_submit,
    ];

    $this->addType($form);

    $this->addSort($form);

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => ' ' . $this->t('Search') . ' ',  // These spaces are just to prevent the whole thing from being replaced by an icon by bulma.theme's bulma_preprocess_input__button__bulma.
      '#name' => '',
      '#attributes' => ['class' => ['is-large fa fa-search']],
      '#button_type' => 'primary',
    ];

    $form['actions']['reset'] = [
      '#markup' => '<a class="reset button is-large is-text" href="/search"><span class="is-small">' . $this->t("Reset") . '</span><span class="icon is-small"><i class="fa fa-times"></i></span></a>',
      '#weight' => 999,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // This form submits to the search page, so processing happens there.
  }

  /**
   * Add virtual opportunities filter element to the form.
   *
   * @param $form
   */
  protected function addVirtual(&$form) {
    // Cast to array to prevent Warning: array_merge(): Argument #2 is not an array
    $query_values = (array) $this->getRequest()->query->get('virtual', []);
    $available_values = array_column($this->query->getFacetMatchesByField('virtual'), 0);
    $virtual_values = array_unique(array_merge($available_values, $query_values));
    // We want people to be able to choose between the two most common options:
    // Show me virtual events.
    // Show me real-life events.
    // The 'online' option indicates an event that is both in-person & virtual,
    // so it is used for both.  TODO find out why this works with common option.
    $virtual_options = [];
    if (in_array('online', $virtual_values) || in_array('online_only', $virtual_values)) {
      $virtual_options['virtual'] = $this->t("Online or over-the-phone");
    }
    if (in_array('false', $virtual_values) || in_array('online', $virtual_values))  {
      $virtual_options['inperson'] = $this->t("In-person");
    }

    if (!empty($virtual_options)) {
      $form['virtual'] = [
        '#settings' => Selectize::settings([
          'placeholder' => t('All opportunities: in-person AND online or over-the-phone'),
          'maxItems' => 1,
        ] + $this->standardSelectizeSettings()),
        '#title' => t('Virtual or In-person'),
        '#options' => $virtual_options,
        '#default_value' => $this->getRequest()->query->get('virtual'),
        '#multiple' => FALSE,
      ] + $this->standardElementBase();
    }
  }

  /**
   * Add opportunity type filter to the form.
   *
   * @param $form
   */
  protected function addType(&$form) {

    // Make the content type tabs.
    $default_types = (array) $this->getRequest()->query->get('types');

    $types_links = [];
    // Mapping of type extras; maybe someday this could get it from some config.
    $type_extras = [
      'findit_program' => [
        'text' => $this->t('Programs'),
      ],
      'findit_event' => [
        'text' => $this->t('Events'),
      ],
      'findit_place' => [
        'text' => $this->t('Places'),
      ],
      'findit_organization' => [
        'text' => $this->t('Organizations'),
      ],
    ];

    $types_checkboxes = [
      '#type' => 'checkboxes',
      '#attributes' => ['class' => ['findit-opportunity-types']],
      '#default_value' => $default_types,
      // For unknown reasons, the usual '#field_suffix' => $inline_submit does not work here.
      // '#suffix' => render($inline_submit), DOES work, but is not ideal and
      // shows up below it not inline so would need work anyway.
    ];

    foreach ($this->query->getFacetMatchesByField('types') as $type) {
      $machine_name = $type[0];
      $query = (array) $this->requestStack->getCurrentRequest()->query->getIterator();
      $query['types'] = $default_types;
      // If it's already selected, we want clicking the link to *remove* it.
      $key = array_search($machine_name, $default_types);
      // Note we could have done this in one line but it would have needed to be
      // if (($key = array_search($machine_name, $default_types)) !== FALSE) {
      // and without that second set of parenthesis protecting the key from
      // being itself set to TRUE... whoo boy, that's a rotten bug.
      $text = $type_extras[$machine_name]['text'];
      $count = $type[1];
      $icon_class = Html::cleanCssIdentifier('icon--' . $machine_name);
      if ($key !== FALSE) {
        $action_class = 'js-findit-type-remove';
        $status_class = 'active';
        // This is critical for accessibility.
        $title_text = $this->t('Filtered to show :type.  Press to remove this constraint.', [':type' => $type_extras[$machine_name]['text']]);
      }
      // If not selected, we want to add it when clicked.
      else {
        $action_class = 'js-findit-type-add';
        $status_class = '';
        // This is critical for accessibility.  @TODO can be better.
        // This could be improved to check the count of types in use and change
        // text to say something like 'Press to show all Programs and Events'
        $title_text = $this->t('Press to filter search results by :type.', [':type' => $type_extras[$machine_name]['text']]);
      }

      // The key is the machine name but we make the value all the HTML
      // prettiness we want, and bizarrely it works.  However, i couldn't stop
      // Drupal from duplicating the key as `types[findit_event]=findit_event`.
      $value = <<<TEMPLATE
<span class="icon--findit $icon_class $action_class $status_class" title="$title_text">
  <span class="icon--findit--count">$count</span>
</span>
<h4 class="title icon--findit--text">$text</h4>
TEMPLATE;
      $types_checkboxes['#options'][$machine_name] = $value;
    }

    if (!isset($types_checkboxes['#options']) && $default_types) {
      foreach ($default_types as $machine_name) {

        $text = $type_extras[$machine_name]['text'];
        $icon_class = Html::cleanCssIdentifier('icon--' . $machine_name);
        $title_text = $this->t('Filtered to show :type.  Press to remove this constraint.', [':type' => $text]);
        $value = <<<TEMPLATE
<span class="icon--findit $icon_class js-findit-type-add" title="$title_text">
  <span class="icon--findit--count">0</span>
</span>
<h4 class="title icon--findit--text">$text</h4>
TEMPLATE;
        $types_checkboxes['#options'][$machine_name] = $value;
      }
    }

    if (isset($types_checkboxes['#options'])) {
      $form['types'] = $types_checkboxes;
    }
  }

  /**
   * Add sorting element to the form.
   *
   * @param $form
   */
  protected function addSort(&$form) {
    $options = [];
    if ($this->isDateContaining()) {
      $options['date.ASC'] = t('Date');
    }
    $options += [
      '' => t('Relevance'),
      'alpha.ASC' => t('Alphabetical'),
    ];

    $form['sort'] = [
      '#title' => t('Sort by'),
      '#options' => $options,
      '#default_value' => $this->getRequest()->query->get('sort'),
      // So long as this is the last item, it looks better without it.
      // '#field_suffix' => $this->inlineSubmit(),
      '#multiple' => FALSE,
      '#settings' => Selectize::settings([
        'placeholder' => t('Relevance'),
        'maxItems' => 1,
      ] + $this->standardSelectizeSettings()),
    ] + $this->standardElementBase();
  }

  /**
   * Return true if the search context makes a date filter or sort sensible.
   *
   * We only show option to sort/filter by date if we are looking at events or
   * programs, or it has already been selected as sort other than relevance.
   */
  protected function isDateContaining() {
    $types = $this->getRequest()->query->get('types') ?: [];
    return (in_array('findit_event', $types, TRUE) || in_array('findit_program', $types, TRUE) || 'date.ASC' == $this->getRequest()->query->get('sort'));
  }

  protected function buildOptions($field) {
    $options = [];
    $vocabulary = 'findit_' . $field;
    $facet_values = array_unique(array_merge(array_column($this->query->getFacetMatchesByField($field), 0), $this->getRequest()->query->get($field, [])));
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vocabulary, 0, NULL, TRUE);
    foreach ($terms as $term) {
      if (in_array($term->name->value, $facet_values)) {
        $options[] = ['text' => $term->name->value, 'value' => $term->name->value, 'weight' => $term->weight->value + 1000, 'synonyms' => $term->field_findit_synonyms->value];
      }
    }
    return $options;
  }

  /**
   * Define a simple button we can re-use next to each filter.
   */
  function inlineSubmit() {
    return [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#name' => '',
      '#button_type' => 'default',
    ];
  }


  /**
   * Define a standard form element we can re-use.
   */
  function standardElementBase() {
    return [
      '#type' => 'selectize',
      '#multiple' => TRUE,
      '#empty_value' => '',
      '#field_suffix' => $this->inlineSubmit(),
    ];
  }

  /**
   * Define standard selectize settings to re-use as defaults.
   */
  function standardSelectizeSettings() {
    return [
      'searchField' => ['text', 'synonyms'],
      'sortField' => ['weight'],
      'closeAfterSelect' => TRUE,
      'maxItems' => 36,
      'plugins' => ['remove_button'],
    ];
  }

  /**
   * From the rich multi-dimensional array used for Selectize options, we
   * provide the fallback options (value => value) used by regular select.
   *
   * @param $options
   *
   * @return array
   */
  function simpleArrayFromValues($options) {
    $flattened_options = [];
    foreach ($options as $option) {
      $flattened_options[$option['value']] = $option['value'];
    }
    return $flattened_options;
  }

}
