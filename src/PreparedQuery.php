<?php


namespace Drupal\drutopia_findit_search;


use Drupal\Core\Language\LanguageManager;
use Drupal\node\NodeInterface;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Query\Query;
use Symfony\Component\HttpFoundation\RequestStack;


class PreparedQuery {

  const LIMIT = 25;

  /**
   * @var \Drupal\search_api\Query
   */
  protected $query;

  public function __construct(RequestStack $request_stack, LanguageManager $language_manager, $index_id) {
    $parameters = $request_stack->getCurrentRequest()->query;
    /* @var $pager_parameters \Drupal\Core\Pager\PagerParametersInterface */
    $pager_parameters = \Drupal::service('pager.parameters');
    $page = $pager_parameters->findPage(1);

    $this->query = new Query(Index::load($index_id));
    $this->query->setLanguages(array_unique(['en', $language_manager->getCurrentLanguage()->getId()]));
    $this->query->setOption('search_api_facets', $this->getFacets());
//    $this->query->sort('registration_dates_end', 'DESC');
    $conditionGroup = $this->query->createConditionGroup('OR', ['nonobsolete']);
    $conditionGroup->addCondition('dates_end', time(), '>');
    $conditionGroup->addCondition('dates_end', NULL);
    $this->query->addConditionGroup($conditionGroup);
    $sort = $parameters->get('sort');
    if ($sort == 'date.ASC') {
      $this->query->sort('findit_next_date', 'ASC');
      $this->query->sort('title', 'ASC');
    }
    // Alphabetical (actually alphanumeric ascending) by entity label.
    elseif ($sort == 'alpha.ASC') {
      $this->query->sort('title', 'ASC');
    }
    else {
      $this->query->sort('search_api_relevance', 'DESC');
      $this->query->sort('title', 'ASC');
    }
    $this->query->setParseMode($this->query->getParseModeManager()->createInstance('sloppy_terms'));

    // Ensure we still get results even if not all searched words are in index.
    $keys = ['#conjunction' => 'OR'];

    $keywords = explode(" ", $parameters->get('keywords', ''));

    // Get each phrase contained in the keyword set starting from the front.
    $phrase_keywords = $keywords;
    while (count($phrase_keywords) > 1) {
      $keys[] = implode(" ", $phrase_keywords);
      array_pop($phrase_keywords);
    }

    // Get each phrase contained in the keyword set starting from the back.
    // $phrase_keywords got 'used up' in last loop so reset it.
    $phrase_keywords = $keywords;
    while (count($phrase_keywords) > 2) {
      array_shift($phrase_keywords);
      $keys[] = implode(" ", $phrase_keywords);
    }
    unset($phrase_keywords);

    // Now include each word, not in any phrase.
    foreach ($keywords as $keyword) {
      $keys[] = $keyword;
    }

    /* The above produces a set of keys that looks like this:
    [
      '#conjunction' => 'OR',
      '"Cambridge Public Health"',
      '"Cambridge Public"',
      '"Public Health"',
      'Cambridge',
      'Public',
      'Health'
    ]
    */
    // @TODO: For longer sets of keywords, catch phrases in the middle.
    // @TODO: Deal with the possibility that people define phrases *themselves*
    // by putting quotation marks "in the query" like that.

    $this->query->keys($keys);

    $this->query->range($page * self::LIMIT, self::LIMIT);
    $this->query->addCondition('status', NodeInterface::PUBLISHED);

    // Convert values from parameters such as 2022-03-27 to timestamps.
    $start = $parameters->get('start');
    $end = $parameters->get('end');
    if (self::validateDate($start) && self::validateDate($end)) {
      $this->query->addCondition('dateranges', [$start, $end], 'BETWEEN');
    }
    elseif (self::validateDate($start)) {
      $this->query->addCondition('dates_end', $start, '>');
    }
    elseif (self::validateDate($end)) {
      $this->query->addCondition('dates', $end, '<');
    }

    $multipleChoiceConditions = [
      'types',
      'ages',
      'neighborhoods',
      'cost_subsidies',
      'activities',
      'services',
    ];

    foreach ($multipleChoiceConditions as $condition) {
      if ($parameters->get($condition) != '') {
        $tag = 'facet:' . $condition;
        $this->query->addConditionGroup($this->query->createConditionGroup('AND', [$tag])->addCondition($condition, $parameters->get($condition), 'IN'));
      }
    }

    // We special-case Virtual opportunities because we want the default to be
    // a union of two parameters, without people having to select both of them.
    $virtual_parameters = [];
    switch ($parameters->get('virtual')) {
      case 'virtual':
        $virtual_parameters[] = 'online_only';
        $virtual_parameters[] = 'online';
        break;
      case 'inperson':
        $virtual_parameters[] = 'false';
        $virtual_parameters[] = 'online';
        break;
      case 'exclude_virtual':
        $virtual_parameters[] = 'false';
      case 'online_only':
        $virtual_parameters[] = 'online_only';
        break;
      default:
        // Do nothing.
    }
    if ($virtual_parameters) {
      $this->query->addConditionGroup($this->query->createConditionGroup('AND', ['facet:virtual'])->addCondition('virtual', $virtual_parameters, 'IN'));
    }

    // $this->query->contains()
    // We want to add additional information to q in order to do boosts with
    // the Lucene engine but that's not possible through search_api it seems so
    // please see drutopia_findit_search.module and its implementation of the
    // alter hook drutopia_findit_search_search_api_solr_query_alter
  }

  /**
   * Returns search results of this query.
   *
   * The entities found by this query are bulk loaded.
   *
   * @return \Drupal\search_api\Query\ResultSetInterface
   * @throws \Drupal\search_api\SearchApiException
   */
  public function getResults() {
    if (!$this->query->hasExecuted()) {
      $results = $this->query->execute();
      $results->preLoadResultItems();

      if ($results->getResultCount() > 0) {
        /* @var $pager_manager \Drupal\Core\Pager\PagerManagerInterface */
        $pager_manager = \Drupal::service('pager.manager');
        $pager = $pager_manager->createPager($results->getResultCount(), self::LIMIT, 1);
        $pager->getCurrentPage();
      }
    }
    else {
      $results = $this->query->getResults();
    }
    return $results;
  }

  /**
   * Returns facet matches of this query for the given field.
   *
   * @param string $field
   *   The field name.
   *
   * @return array
   * @throws \Drupal\search_api\SearchApiException
   */
  public function getFacetMatchesByField($field) {
    $facet_fields = $this->getResults()->getExtraData('search_api_facets');
    $map = function ($item) {
      return [trim($item['filter'], '"'), $item['count']];
    };
    if (is_array($facet_fields) && array_key_exists($field, $facet_fields)) {
      return array_map($map, $facet_fields[$field]);
    }
    else {
      return [];
    }
  }

  /**
   * Returns facets of this query.
   *
   * @return array
   */
  public function getFacets() {
    return [
      'types' => [
        'field' => 'types',
        'limit' => 50,
        'operator' => 'or',
        'min_count' => 1,
        'missing' => FALSE,
      ],
      'neighborhoods' => [
        'field' => 'neighborhoods',
        'limit' => 50,
        'operator' => 'or',
        'min_count' => 1,
        'missing' => FALSE,
      ],
      'virtual' => [
        'field' => 'virtual',
        'limit' => 50,
        'operator' => 'or',
        'min_count' => 1,
        'missing' => FALSE,
      ],
      'cost_subsidies' => [
        'field' => 'cost_subsidies',
        'limit' => 50,
        'operator' => 'or',
        'min_count' => 1,
        'missing' => FALSE,
      ],
      'activities' => [
        'field' => 'activities',
        'limit' => 50,
        'operator' => 'or',
        'min_count' => 1,
        'missing' => FALSE,
      ],
      'services' => [
        'field' => 'services',
        'limit' => 50,
        'operator' => 'or',
        'min_count' => 1,
        'missing' => FALSE,
      ],
      'ages' => [
        'field' => 'ages',
        'limit' => 50,
        'operator' => 'or',
        'min_count' => 1,
        'missing' => FALSE,
      ],
    ];
  }

  public static function validateDate($date, $format = 'Y-m-d') {
    $test_date = \DateTime::createFromFormat($format, $date);
    return $test_date && $test_date->format($format) === $date;
  }

}
