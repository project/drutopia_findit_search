<?php

namespace Drupal\drutopia_findit_search\Plugin\search_api\processor;

use Drupal\search_api\Processor\ProcessorPluginBase;

/**
 * Adds a boost to indexed items based on their datasource and/or bundle.
 *
 * @SearchApiProcessor(
 *   id = "findit_manual_boost",
 *   label = @Translation("Manual per-opportunity boosting"),
 *   description = @Translation("Adds a boost to indexed items based on a site manager having set one for the specific item."),
 *   stages = {
 *     "preprocess_index" = 0,
 *   }
 * )
 */
class ManualBoost extends ProcessorPluginBase {

  /**
   * The available boost factors.
   *
   * @var string[]
   */
  protected static $boost_factors = [
    'slight' => '2.0',
    '3.0' => '3.0',
    'strong' => '5.0',
    '8.0' => '8.0',
    '13.0' => '13.0',
    'pin' => '21.0',
  ];


  /**
   * {@inheritdoc}
   */
  public function preprocessIndexItems(array $items) {

    /** @var \Drupal\search_api\Item\ItemInterface $item */
    foreach ($items as $item) {
      $opportunity = $item->getOriginalObject()->getEntity();
      $manual_boost = $opportunity->field_findit_boost->value;
      if ($manual_boost) {
        $item_boost = (double) static::$boost_factors[$manual_boost];
        $item->setBoost($item->getBoost() * $item_boost);
      }

    }
  }

}
