<?php


namespace Drupal\drutopia_findit_search\Plugin\search_api\processor;


use DateTime;
use DateTimeZone;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\Entity\Node;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Adds our derived fields.
 *
 * @SearchApiProcessor(
 *   id = "addderivedfields",
 *   label = @Translation("Add derived fields"),
 *   description = @Translation("Add fields from related objetcs and provide defaults for missing fields."),
 *   stages = {
 *     "add_properties" = -30,
 *     "preprocess_index" = -20,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class AddDerivedFields extends ProcessorPluginBase {

  /**
   * @var EntityTypeManager $entityTypeManager
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /* @var AddDerivedFields $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $processor->setEntityTypeManager($container->get('entity_type.manager'));
    return $processor;
  }

  public function setEntityTypeManager(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Next date'),
        'description' => $this->t('Very next date, only one.'),
        'type' => 'date',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['findit_next_date'] = new ProcessorProperty($definition);
    }

    return $properties;
  }


  /**
   * Adds fields to index items.
   *
   * - default value for cost subsidies to place items
   * - services and activities to organization items taken from the
   *   associated programs and events
   * - set places as not available online
   * - set events and programs not explicitly virtual as not available online.
   *
   * @param \Drupal\search_api\Item\Item[] $items
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\search_api\SearchApiException
   */
  public function preprocessIndexItems(array $items) {
    foreach ($items as $item) {
      switch ($item->getOriginalObject()->getValue('entity')->bundle()) {
        case 'findit_place':
          $field = $this->getFieldsHelper()->createField($item->getIndex(), 'cost_subsidies', ['type' => 'string']);
          $field->addValue('free');
          $item->setField('cost_subsidies', $field);
          // Add all ages.
          $age_terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('findit_ages');
          $field = $this->getFieldsHelper()->createField($item->getIndex(), 'ages', ['type' => 'string']);
          foreach ($age_terms as $term) {
            $field->addValue($term->name);
          }
          $item->setField('ages', $field);
          // Add not online (virtual set to false)
          $field = $this->getFieldsHelper()->createField($item->getIndex(), 'virtual', ['type' => 'string']);
          $field->addValue('false');
          $item->setField('virtual', $field);
          break;
        case 'findit_organization':
          $field = $this->getFieldsHelper()->createField($item->getIndex(), 'services', ['type' => 'string']);
          $field->setValues($this->getTermsFromProgramsAndEvents($item->getOriginalObject()->getValue('entity'), 'field_findit_services'));
          $item->setField('services', $field);
          $field = $this->getFieldsHelper()->createField($item->getIndex(), 'activities', ['type' => 'string']);
          $field->setValues($this->getTermsFromProgramsAndEvents($item->getOriginalObject()->getValue('entity'), 'field_findit_activities'));
          $item->setField('activities', $field);
          break;
        case 'findit_program':
          $node = $item->getOriginalObject()->getValue('entity');
          $where = $node->get('field_findit_where')->getString();
          if (in_array($where, ['where_needed', 'time_dependent'])) {
            $field = $item->getField('neighborhoods');
            $field->setValues($this->getNeighborhoods());
            $item->setField('neighborhoods', $field);
          }
          // Intentionally no break; we want next date added for programs & events.
        case 'findit_event':
          $next_date = $this->getNextDate($item->getOriginalObject()->getValue('entity'));
          $field = $this->getFieldsHelper()->createField($item->getIndex(), 'findit_next_date', ['type' => 'date']);
          $field->addValue($next_date);
          $item->setField('findit_next_date', $field);
          // Add not online to programs and events without value for virtual.
          if (!$item->getOriginalObject()->getValue('entity')->field_findit_virtual->getString()) {
            $field = $this->getFieldsHelper()->createField($item->getIndex(), 'virtual', ['type' => 'string']);
            $field->addValue('false');
            $item->setField('virtual', $field);
          }
      }
    }
  }

  /**
   * Returns terms from programs and events of the given organization.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The organization.
   *
   * @param string $field_name
   *   The name of the taxonomy term reference field.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @return array
   *   The names of terms from all the associated programs and events.
   */
  protected function getTermsFromProgramsAndEvents(Node $node, $field_name) {
    $terms = [];

    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $node_storage = $this->entityTypeManager->getStorage('node');

    $q = $node_storage->getQuery();
    $q->condition('type', ['findit_program', 'findit_event'], 'IN');
    $q->condition('field_findit_organization', $node->id());
    $q->condition('status', Node::PUBLISHED);

    foreach ($node_storage->loadMultiple($q->execute()) as $node) {
      foreach ($term_storage->loadMultiple(array_column($node->$field_name->getValue(), 'target_id')) as $term) {
        $terms[] = $term->name->value;
      }
    }

    return array_unique($terms);
  }

  /**
   * Returns all available neighborhoods.
   */
  protected function getNeighborhoods($cambridge_only = TRUE) {
    $neighborhood_terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree('findit_neighborhoods');
    $neighborhoods = array_column($neighborhood_terms, 'name');
    // @TODO make setting for excluded terms and not hard-code for Cambridge.
    if ($cambridge_only) {
      unset($neighborhoods[array_search('Outside Cambridge', $neighborhoods)]);
    }
    return $neighborhoods;
  }

  /**
   * Return the next upcoming date for events and programs.
   *
   * This means the entire opportunity content must be re-indexed daily.
   */
  protected function getNextDate(Node $node) {
    // We're supposed to show the events happening *today* even if they are
    // past, so this can't be relative to the current time but the actual day.
    // @TODO make timezone configurable.
    $date = new DateTime('today', new DateTimeZone('America/New_York'));
    $utc_date = new DateTime($date->format('@U'), new DateTimeZone('UTC'));
    $today = $utc_date->getTimestamp(); // was: format(DATE_ISO8601);
    // We are going to presume that the dates are in order to save work here.
    // @TODO force ordering of multivalue dates on save.
    foreach ($node->field_findit_opportunity_dates as $date) {
      if ($date->end_value > $today) {
        return $date->value;
      }
    }
  }

}
