<?php


namespace Drupal\drutopia_findit_search\Plugin\Block;


use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\drutopia_findit_search\PreparedQuery;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * @Block(
 *   id = "findit_filters_block",
 *   admin_label = @Translation("Filters"),
 *   category = @Translation("Search"),
 * )
 */
class FiltersBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var PreparedQuery
   */
  protected $query;

  /**
   * @var RequestStack
   */
  protected $requestStack;

  /**
   * FiltersBlock constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\drutopia_findit_search\PreparedQuery $query
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PreparedQuery $query, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->query = $query;
    $this->requestStack = $request_stack;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('drutopia_findit_search.prepared_query'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $form = \Drupal::formBuilder()->getForm('Drupal\drutopia_findit_search\Form\FiltersForm');
    $form['form_build_id']['#access'] = FALSE;
    $form['form_token']['#access'] = FALSE;
    $form['form_id']['#access'] = FALSE;
    $build['form'] = $form;
    $types = [];
    foreach ($this->query->getFacetMatchesByField('types') as $type) {
      $machine_name = $type[0];
      $query = (array) $this->requestStack->getCurrentRequest()->query->getIterator();
      $query['type'] = $machine_name;
      $url = Url::createFromRequest($this->requestStack->getCurrentRequest())->mergeOptions(['query' => $query]);
      $types[] = [
        '#theme' => 'findit_search_tab',
        '#url' => $url,
        '#text' => $machine_name,
        '#count' => $type[1],
        '#icon' => $machine_name,
      ];
    }

//    $build['types'] = $types;
    return $build;
  }

  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path', 'url.query_args']);
  }

}
