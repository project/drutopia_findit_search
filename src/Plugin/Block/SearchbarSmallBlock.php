<?php


namespace Drupal\drutopia_findit_search\Plugin\Block;


use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;


/**
 * @Block(
 *   id = "findit_searchbar_small_block",
 *   admin_label = @Translation("Search form (small)"),
 *   category = @Translation("Forms")
 * )
 */
class SearchbarSmallBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\drutopia_findit_search\Form\SearchbarSmallBlockForm');
    $form['form_build_id']['#access'] = FALSE;
    $form['form_token']['#access'] = FALSE;
    $form['form_id']['#access'] = FALSE;
    return $form;
  }

}
