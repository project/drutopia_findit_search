<?php

namespace Drupal\drutopia_findit_search\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\drutopia_findit_search\MicroOccurrenceMarkupTrait;
use Drupal\search_api\Item\Item;


/**
 * @Block(
 *   id = "findit_search_todays_events",
 *   admin_label = @Translation("Today's events"),
 *   category = @Translation("Content lists")
 * )
 */
class TodaysEvents extends BlockBase {

  use MicroOccurrenceMarkupTrait;

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $todays_events = \Drupal::service('drutopia_findit_search.todays_events');
    $result = $todays_events->getResults();
    if ($result->getResultCount() == 0) {
      $build = [];
      $build['none_today'] = [
        '#markup' => '<p class="drum">' . t('No events today.') . '</p>',
      ];
    }
    else {
      $occurences = MicroOccurrenceMarkupTrait::getDaysOccurrences($result, $todays_events->today_start->getTimestamp(), $todays_events->today_end->getTimestamp());
      $build['body'] = [
        '#markup' => MicroOccurrenceMarkupTrait::renderDay($occurences),
      ];
    }
    $build['all_events_link'] = [
      '#markup' => '<h3 class="title">' . '<a href="/upcoming-events">' . t('See all upcoming events.') . '</a>' . '</h3>',
    ];
    return $build;

  }

}
